// watch
$(document).ready(function() {
    var slider = $('#autoWidth').lightSlider({
        autoWidth: true,
        loop: false,
        onSliderLoad: function() {
            $('#autoWidth').removeClass('cS-hidden');
            $('.lSPrev').hide(); // Hide the previous button on slider load
        }
    });
    $('.lSNext').click(function() {
        $('.lSPrev').show();
    });
    $('.lSPrev').click(function() {
        $('.lSNext').show();
    });
    // Add an event listener for clicking the next button
    $('.lSNext').on('click', function() {
        console.log(slider.getCurrentSlideCount())
        var currentSlideIndex = slider.getCurrentSlideCount();

        if (currentSlideIndex ===  4) {
            $('.lSNext').hide();
        }
    });
    $('.lSPrev').on('click', function() {
        console.log(slider.getCurrentSlideCount())
        var currentSlideIndex = slider.getCurrentSlideCount() ;

        if (currentSlideIndex === 1) {
            $('.lSPrev').hide();
        }
    });
    $('.productdiv').addClass('no-padding');
    $('.lSNext').click(function() {
        var currentSlideIndex = slider.getCurrentSlideCount();
        console.log("Current Slide Index:", currentSlideIndex);

        // Check if the current slide index is 2
        if (currentSlideIndex === 2) {
            // Add a class to hide padding from elements with the class 'productdiv'
            $('.productdiv').removeClass('no-padding');
            
        }
    });
    $('.lSPrev').click(function() {
        var currentSlideIndex = slider.getCurrentSlideCount();
        console.log("Current Slide Index:", currentSlideIndex);

        // Check if the current slide index is 2
        if (currentSlideIndex === 1) {
            // Add a class to hide padding from elements with the class 'productdiv'
            $('.productdiv').addClass('no-padding');
        }
    });

});

//iphone
$(document).ready(function() {
    var slider = $('#autoWidth1').lightSlider({
        autoWidth: true,
        loop: false,
        onSliderLoad: function() {
            $('#autoWidth').removeClass('cS-hidden');
            $('.lSPrev').hide(); // Hide the previous button on slider load
        }
    });
    $('.lSNext').click(function() {
        $('.lSPrev').show();
    });
    $('.lSPrev').click(function() {
        $('.lSNext').show();
    });
    // Add an event listener for clicking the next button
    $('.lSNext').on('click', function() {
        console.log(slider.getCurrentSlideCount())
        var currentSlideIndex = slider.getCurrentSlideCount();

        if (currentSlideIndex ===  4) {
            $('.lSNext').hide();
        }
    });
    $('.lSPrev').on('click', function() {
        console.log(slider.getCurrentSlideCount())
        var currentSlideIndex = slider.getCurrentSlideCount() ;

        if (currentSlideIndex === 1) {
            $('.lSPrev').hide();
        }
    });
    $('.productdiv').addClass('no-padding');
    $('.lSNext').click(function() {
        var currentSlideIndex = slider.getCurrentSlideCount();
        console.log("Current Slide Index:", currentSlideIndex);

        // Check if the current slide index is 2
        if (currentSlideIndex === 2) {
            // Add a class to hide padding from elements with the class 'productdiv'
            $('.productdiv').removeClass('no-padding');
            
        }
    });
    $('.lSPrev').click(function() {
        var currentSlideIndex = slider.getCurrentSlideCount();
        console.log("Current Slide Index:", currentSlideIndex);

        // Check if the current slide index is 2
        if (currentSlideIndex === 1) {
            // Add a class to hide padding from elements with the class 'productdiv'
            $('.productdiv').addClass('no-padding');
        }
    });

});

// tab
$(document).ready(function() {
    var slider = $('#autoWidth2').lightSlider({
        autoWidth: true,
        loop: false,
        onSliderLoad: function() {
            $('#autoWidth').removeClass('cS-hidden');
            $('.lSPrev').hide(); // Hide the previous button on slider load
        }
    });
    $('.lSNext').click(function() {
        $('.lSPrev').show();
    });
    $('.lSPrev').click(function() {
        $('.lSNext').show();
    });
    // Add an event listener for clicking the next button
    $('.lSNext').on('click', function() {
        console.log(slider.getCurrentSlideCount())
        var currentSlideIndex = slider.getCurrentSlideCount();

        if (currentSlideIndex ===  4) {
            $('.lSNext').hide();
        }
    });
    $('.lSPrev').on('click', function() {
        console.log(slider.getCurrentSlideCount())
        var currentSlideIndex = slider.getCurrentSlideCount() ;

        if (currentSlideIndex === 1) {
            $('.lSPrev').hide();
        }
    });
    $('.productdiv').addClass('no-padding');
    $('.lSNext').click(function() {
        var currentSlideIndex = slider.getCurrentSlideCount();
        console.log("Current Slide Index:", currentSlideIndex);

        // Check if the current slide index is 2
        if (currentSlideIndex === 2) {
            // Add a class to hide padding from elements with the class 'productdiv'
            $('.productdiv').removeClass('no-padding');
            
        }
    });
    $('.lSPrev').click(function() {
        var currentSlideIndex = slider.getCurrentSlideCount();
        console.log("Current Slide Index:", currentSlideIndex);

        // Check if the current slide index is 2
        if (currentSlideIndex === 1) {
            // Add a class to hide padding from elements with the class 'productdiv'
            $('.productdiv').addClass('no-padding');
        }
    });

});

//mac
$(document).ready(function() {
    var slider = $('#autoWidth3').lightSlider({
        autoWidth: true,
        loop: false,
        onSliderLoad: function() {
            $('#autoWidth').removeClass('cS-hidden');
            $('.lSPrev').hide(); // Hide the previous button on slider load
        }
    });
    $('.lSNext').click(function() {
        $('.lSPrev').show();
    });
    $('.lSPrev').click(function() {
        $('.lSNext').show();
    });
    // Add an event listener for clicking the next button
    $('.lSNext').on('click', function() {
        console.log(slider.getCurrentSlideCount())
        var currentSlideIndex = slider.getCurrentSlideCount();

        if (currentSlideIndex ===  4) {
            $('.lSNext').hide();
        }
    });
    $('.lSPrev').on('click', function() {
        console.log(slider.getCurrentSlideCount())
        var currentSlideIndex = slider.getCurrentSlideCount() ;

        if (currentSlideIndex === 1) {
            $('.lSPrev').hide();
        }
    });
    $('.productdiv').addClass('no-padding');
    $('.lSNext').click(function() {
        var currentSlideIndex = slider.getCurrentSlideCount();
        console.log("Current Slide Index:", currentSlideIndex);

        // Check if the current slide index is 2
        if (currentSlideIndex === 2) {
            // Add a class to hide padding from elements with the class 'productdiv'
            $('.productdiv').removeClass('no-padding');
            
        }
    });
    $('.lSPrev').click(function() {
        var currentSlideIndex = slider.getCurrentSlideCount();
        console.log("Current Slide Index:", currentSlideIndex);

        // Check if the current slide index is 2
        if (currentSlideIndex === 1) {
            // Add a class to hide padding from elements with the class 'productdiv'
            $('.productdiv').addClass('no-padding');
        }
    });

});


// watch
$(document).ready(function() {
    var slider = $('#autoWidth4').lightSlider({
        autoWidth: true,
        loop: false,
        onSliderLoad: function() {
            $('#autoWidth').removeClass('cS-hidden');
            $('.lSPrev').hide(); // Hide the previous button on slider load
        }
    });
    $('.lSNext').click(function() {
        $('.lSPrev').show();
    });
    $('.lSPrev').click(function() {
        $('.lSNext').show();
    });
    // Add an event listener for clicking the next button
    $('.lSNext').on('click', function() {
        console.log(slider.getCurrentSlideCount())
        var currentSlideIndex = slider.getCurrentSlideCount();

        if (currentSlideIndex ===  4) {
            $('.lSNext').hide();
        }
    });
    $('.lSPrev').on('click', function() {
        console.log(slider.getCurrentSlideCount())
        var currentSlideIndex = slider.getCurrentSlideCount() ;

        if (currentSlideIndex === 1) {
            $('.lSPrev').hide();
        }
    });
    $('.productdiv').addClass('no-padding');
    $('.lSNext').click(function() {
        var currentSlideIndex = slider.getCurrentSlideCount();
        console.log("Current Slide Index:", currentSlideIndex);

        // Check if the current slide index is 2
        if (currentSlideIndex === 2) {
            // Add a class to hide padding from elements with the class 'productdiv'
            $('.productdiv').removeClass('no-padding');
            
        }
    });
    $('.lSPrev').click(function() {
        var currentSlideIndex = slider.getCurrentSlideCount();
        console.log("Current Slide Index:", currentSlideIndex);

        // Check if the current slide index is 2
        if (currentSlideIndex === 1) {
            // Add a class to hide padding from elements with the class 'productdiv'
            $('.productdiv').addClass('no-padding');
        }
    });

});
// airpod
$(document).ready(function() {
    var slider = $('#autoWidth5').lightSlider({
        autoWidth: true,
        loop: false,
        onSliderLoad: function() {
            $('#autoWidth').removeClass('cS-hidden');
            $('.lSPrev').hide(); // Hide the previous button on slider load
        }
    });
    $('.lSNext').click(function() {
        $('.lSPrev').show();
    });
    $('.lSPrev').click(function() {
        $('.lSNext').show();
    });
    // Add an event listener for clicking the next button
    $('.lSNext').on('click', function() {
        console.log(slider.getCurrentSlideCount())
        var currentSlideIndex = slider.getCurrentSlideCount();

        if (currentSlideIndex ===  4) {
            $('.lSNext').hide();
        }
    });
    $('.lSPrev').on('click', function() {
        console.log(slider.getCurrentSlideCount())
        var currentSlideIndex = slider.getCurrentSlideCount() ;

        if (currentSlideIndex === 1) {
            $('.lSPrev').hide();
        }
    });
    $('.productdiv').addClass('no-padding');
    $('.lSNext').click(function() {
        var currentSlideIndex = slider.getCurrentSlideCount();
        console.log("Current Slide Index:", currentSlideIndex);

        // Check if the current slide index is 2
        if (currentSlideIndex === 2) {
            // Add a class to hide padding from elements with the class 'productdiv'
            $('.productdiv').removeClass('no-padding');
            
        }
    });
    $('.lSPrev').click(function() {
        var currentSlideIndex = slider.getCurrentSlideCount();
        console.log("Current Slide Index:", currentSlideIndex);

        // Check if the current slide index is 2
        if (currentSlideIndex === 1) {
            // Add a class to hide padding from elements with the class 'productdiv'
            $('.productdiv').addClass('no-padding');
        }
    });

});
// all
$(document).ready(function() {
    var slider = $('#autoWidth6').lightSlider({
        autoWidth: true,
        loop: false,
        onSliderLoad: function() {
            $('#autoWidth').removeClass('cS-hidden');
            $('.lSPrev').hide(); // Hide the previous button on slider load
        }
    });
    $('.lSNext').click(function() {
        $('.lSPrev').show();
    });
    $('.lSPrev').click(function() {
        $('.lSNext').show();
    });
    // Add an event listener for clicking the next button
    $('.lSNext').on('click', function() {
        console.log(slider.getCurrentSlideCount())
        var currentSlideIndex = slider.getCurrentSlideCount();

        if (currentSlideIndex ===  4) {
            $('.lSNext').hide();
        }
    });
    $('.lSPrev').on('click', function() {
        console.log(slider.getCurrentSlideCount())
        var currentSlideIndex = slider.getCurrentSlideCount() ;

        if (currentSlideIndex === 1) {
            $('.lSPrev').hide();
        }
    });
    $('.productdiv').addClass('no-padding');
    $('.lSNext').click(function() {
        var currentSlideIndex = slider.getCurrentSlideCount();
        console.log("Current Slide Index:", currentSlideIndex);

        // Check if the current slide index is 2
        if (currentSlideIndex === 2) {
            // Add a class to hide padding from elements with the class 'productdiv'
            $('.productdiv').removeClass('no-padding');
            
        }
    });
    $('.lSPrev').click(function() {
        var currentSlideIndex = slider.getCurrentSlideCount();
        console.log("Current Slide Index:", currentSlideIndex);

        if (currentSlideIndex === 1) {
            $('.productdiv').addClass('no-padding');
        }
    });
});
$(document).ready(function() {
    $(".toggleprofile").click(function() {
        $(".popupmenu").fadeToggle(500); // 500 milliseconds for the fade animation
    });
});
$(document).ready(function() {
    $(".popup").click(function() {
        $(".popupmenu").css("display", "none");
    });
});
$(document).ready(function(){
    $(".inputbox").on("focus", function(){
        $("body").css("overflow", "hidden"); 
        $("body").addClass("dim");
        $(".search").addClass("white");
    });

    $(".inputbox").on("blur", function(){
        $(this).removeClass("dim");
        $("body").removeClass("dim");
        $(".search").removeClass("white");
        $("body").css("overflow", "auto");
    });
});
// login page
$(document).ready(function() {
    $(".login").click(function() {
        $(".display-login").fadeToggle(300); // 500 milliseconds for the fade animation
        $("body").css("overflow", "hidden"); 
        $("body").addClass("dim");
        $(".inputbox").addClass("dim1");
        $(".herobanner").addClass("dim");
        $(".bannerwording").addClass("dim");
    });
});
$(document).ready(function() {
    $(".close-button").click(function() {
        $(".display-signup").fadeOut(300);
        $("body").css("overflow", "auto");
        $("body").removeClass("dim");
        $(".inputbox").removeClass("dim1");
        $(".herobanner").removeClass("dim");
        $(".bannerwording").removeClass("dim");
    });
});
// sign up
$(document).ready(function() {
    $(".signup").click(function() {
        $(".display-signup").fadeToggle(300); // 500 milliseconds for the fade animation
        $("body").css("overflow", "hidden"); 
        $("body").addClass("dim");
        $(".inputbox").addClass("dim1");
        $(".herobanner").addClass("dim");
        $(".bannerwording").addClass("dim");
    });
});
$(document).ready(function() {
    $(".close-button").click(function() {
        $(".display-login").fadeOut(300);
        $("body").css("overflow", "auto");
        $("body").removeClass("dim");
        $(".inputbox").removeClass("dim1");
        $(".herobanner").removeClass("dim");
        $(".bannerwording").removeClass("dim");
    });
});

// Captcha page
$(document).ready(function() {
    $(".next").click(function() {
        $(".display-otp").fadeToggle(300); // 500 milliseconds for the fade animation
        $("body").css("overflow", "hidden"); 
        $("body").addClass("dim");
        $(".inputbox").addClass("dim1");
        $(".herobanner").addClass("dim");
        $(".bannerwording").addClass("dim");
        $(".display-login").fadeOut(300);
    });
});
$(document).ready(function() {
    $(".close-button").click(function() {
        $(".display-otp").fadeOut(300);
        $("body").css("overflow", "auto");
        $("body").removeClass("dim");
        $(".inputbox").removeClass("dim1");
        $(".herobanner").removeClass("dim");
        $(".bannerwording").removeClass("dim");
    });
});
function generateRandomCaptcha() {
    var captchaLength = 6;
    var captchaContent = '';

    var characters = 'ABCDEFGHIJKLMNPQRSTUVWXYZabcdefghijklmnpqrstuvwxyz0123456789';

    for (var i = 0; i < captchaLength; i++) {
        var randomIndex = Math.floor(Math.random() * characters.length);
        var char = characters.charAt(randomIndex);
        var isLetter = /[A-Za-z]/.test(char);

        captchaContent += '<span class="captcha-char ' + (isLetter ? 'upside-down' : '') + ' ' + (isLetter ? 'slant' : '') + '">' + char + '</span>';
    }

    return captchaContent;
}
function refreshCaptcha() {
    var captchaInput = $("#captchaInput");
    captchaInput.html(generateRandomCaptcha());
}
$(document).ready(function() {
    refreshCaptcha();
    $("#refreshLink").on("click", function(e) {
        e.preventDefault();
        refreshCaptcha();
    });
});
$(document).ready(function(){
    $('.next2').click(function(){
        var enteredText = $(".captchafield").val();
        var captchaText = $("#captchaInput").text().replace(/\s/g, '');

        if (enteredText === captchaText) {
            $(".display-otp1").fadeToggle(300);// Show the .otp-page
        }else{
            alert("wrong")
        }
    });
});

// otp
$(document).ready(function() {
    $(".next1").click(function() {
        $(".display-otp1").fadeToggle(300); // 500 milliseconds for the fade animation
        $("body").css("overflow", "hidden"); 
        $("body").addClass("dim");
        $(".inputbox").addClass("dim1");
        $(".herobanner").addClass("dim");
        $(".bannerwording").addClass("dim");
        $(".display-otp").fadeOut(300);
    });
});
$(document).ready(function() {
    $(".close-button").click(function() {
        $(".display-otp1").fadeOut(300);
        $("body").css("overflow", "auto");
        $("body").removeClass("dim");
        $(".inputbox").removeClass("dim1");
        $(".herobanner").removeClass("dim");
        $(".bannerwording").removeClass("dim");
    });
});
$(document).ready(function() {
    const $inputContainer = $('.input-container');
    const $inputField = $('#inputField');

    $inputContainer.click(function() {
        $inputContainer.addClass('active');
    });

    $(document).click(function(e) {
        if (!$inputField.val() && !$inputContainer.is(e.target) && $inputContainer.has(e.target).length === 0) {
            $inputContainer.removeClass('active');
        }
    });
    $inputField.on('input', function() {
        if ($inputField.val()) {
            $inputContainer.addClass('active');
        } else {
            $inputContainer.removeClass('active');
        }
    });
});

$(document).ready(function() {
    const $inputContainer = $('.input-container2');
    const $inputField = $('#inputField2');

    $inputContainer.click(function() {
        $inputContainer.addClass('active');
    });

    $(document).click(function(e) {
        if (!$inputField.val() && !$inputContainer.is(e.target) && $inputContainer.has(e.target).length === 0) {
            $inputContainer.removeClass('active');
        }
    });

    $inputField.on('input', function() {
        if ($inputField.val()) {
            $inputContainer.addClass('active');
        } else {
            $inputContainer.removeClass('active');
        }
    });
});
$(document).ready(function() {
    const $inputContainer = $('.input-container3');
    const $inputField = $('#inputField3');

    $inputContainer.click(function() {
        $inputContainer.addClass('active');
    });

    $(document).click(function(e) {
        if (!$inputField.val() && !$inputContainer.is(e.target) && $inputContainer.has(e.target).length === 0) {
            $inputContainer.removeClass('active');
        }
    });

    $inputField.on('input', function() {
        if ($inputField.val()) {
            $inputContainer.addClass('active');
        } else {
            $inputContainer.removeClass('active');
        }
    });
});